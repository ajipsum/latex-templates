#!/bin/bash

SCRIPT="$(readlink -f "$0")"
SCRIPTPATH="$(dirname "$SCRIPT")"

cp -r "$SCRIPTPATH/includes" "$(pwd)"
cp "$SCRIPTPATH/article.tex" "$(pwd)"
