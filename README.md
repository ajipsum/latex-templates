UTCS LaTeX Templates
====================

A set of LaTeX Templates intended for use in the Department of Computer Science
at the University of Texas at Austin, designed to comply with visual style
rules set by UT (where appropriate).

Most of these are not my original work, but (as so often happens with LaTeX)
was scraped off of the internet and from colleagues and modified to suit my 
needs. A full list of credits can be found at the end of this page.

# A Brief Tour

Here is a brief tour of select visuals and features of the templates.

## Presentations

The presentation follows the color palette specified by the
[UT Visual Guidelines](http://brand.utexas.edu/visual-identity/colors) and
uses the OpenSans font family as 
[suggested for web publications](http://brand.utexas.edu/visual-identity/fonts).

<img src="imgs/p1.png" width="49%" />
<img src="imgs/p2.png" width="49%" />

The template contains a built-in mechanism for dealing with citations. If you
don't like it (or you don't have the dependencies), disabling it is as simple
as passing an option to the theme.

The built-in citation system will automatically abbreviate any repeated
citations while hyperlinking them back to the full citation. The alert
blocks are, like the rest of the template, using UT-approved colors
(in this case, from the secondary palette).

<img src="imgs/p4.png" width="49%" />
<img src="imgs/p5.png" width="49%" />

And of course, if you so desire, you can create a final slide with a giant
UTCS logo on it.

## Quick Articles

The article template features commands to make a quick one-off document with
minimum hassle. It has a huge selection of easy math operators thanks to the
mathstub, as well as defaults for the listings package which make your code
look somewhat decent.

<p align="center">
<img src="imgs/a1.png" width="65%" />
</p>

To quickly spawn an instance of this article (along with the prerequisite
includefiles) in your current directory (i.e. `pwd`), simply run 
`article_create.sh`.

# Quick Build

To build the demos yourself, build `article.tex`
and `presentation.tex` with a pdf-generating tool, e.g.

```
latexmk -pdf article.tex presentation.tex
```

Note 1: On UTCS Ubuntu 16.04 machines, `biber` and `biblatex` are
incompatible versions. Until this is fixed, the best solution is probably to
[download biber 2.4](https://sourceforge.net/projects/biblatex-biber/files/biblatex-biber/2.4/)
and use that in the standard `pdflatex`, `biber`, `pdflatex`, `pdflatex` loop.

Note 2: Some versions of latexmk claim to fail, but will actually generate a full PDF when 
working with biber. I'm not sure why this is yet.

# Usage

#### Presentation

To use the presentation template, you will need to copy `presentation.tex`, 
`mathstub.tex`, and the `figs` directory to wherever you want to work
on them. Then modify `presentation.tex` however you'd like.

You need to build the presentation using PDF tools due to how hyperref works.
If you're using `latexmk`, make sure you pass the `-pdf` flag.

If you want to disable the built-in citation management, pass `nousebiblatex`
as an option to the `\usetheme` command. If you want to enable the navbar,
pass the `navbar` option.

#### Article

To use the article template, you only need to copy `article.tex` and 
`mathstub.tex` to where you want to work on them. Then modify
`article.tex` however you'd like.

# A Little More about some files

### Math Template (mathstub.tex)

Contains a bunch of really useful shortcuts for math symbols. Automatically
imports the following packages for maximum mathematical processing:

  - eqparbox
  - amsmath
  - amsthm
  - amsfonts
  - amssymb
  - bbm
  - leftidx
  - mathtools

It defines the following shortcut families:

  - All uppercase blackboard letters can be typeset with a prepended `\bb`, e.g.
    `\mathbb{R}` becomes `\bbR`. In addition, `\mathbbm{1}` can be replaced with
    `\bbone.`
    
  - Most boldface symbols can be typeset with a prepended `\bf`. This includes
    all upper and lower case letters and some greek symbols, e.g. `\bfOmega`

  - All upper case calligraphic letters can be typeset with the prepended `\cal`.
  
  - Defines several common statistical operators and group symbols so that they
    will be typeset in Roman font.
    
  - Defines the `\dif` macro to take care of typesetting the d in differentials
  
  - Defines several paired delimiters if you prefer to use them that way.
  
You can look in the template around line 20 (look for the line starting with
`%%operators`) to see all the operators that are defined
  
### Article Example (article.tex)

Contains code to make listings less ugly by default.

This template also contains most of the examples of how use the features in the
math template. If you want to get an idea of how to take advantage of the math
shortcuts, compile this file.

### Beamer Theme (beamerthemeutcs.sty)

The file for the utcs beamer theme. Relies heavily on the work of John T. Foster
from Cockrell School of Engineering.

The theme has its own reference management. In order to use it, you
will need to have [biber](http://biblatex-biber.sourceforge.net/) installed.
If you cannot access/install biber, you can pass
the option `nousebiblatex` into the the theme's options list. This will
require you to manage your own citations (e.g. using natbib).

Note that any attempts to build a presentation based off of this theme
must use a pdf-based renderer. Latexmk users should make sure to use the
`-pdf` flag or funky errors will result.

Options to the theme include `usebiblatex`/`nousebiblatex` as described
above, and `navbar`/`nonavbar` to enable/disable the navigation bar.

# Credits

The math template was given to me by Chi-Kit (George) Lam during CS395T. It has
been augmented with additional commands and operators, but it remains
relatively close to the original.

The Beamer theme was taken from 
[GitHub](https://github.com/johntfoster/cockrell-school-latex-beamer-template)
and modified to use the UTCS seals and logos. This theme would not exist were
it not for Foster's work.

Louis Long Ly of ICES provided the vector graphics used in the Beamer theme.

Several short macros were borrowed from GitHub user [nykh](https://github.com/nykh).
